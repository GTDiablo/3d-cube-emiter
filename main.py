from typing import Dict, NewType
from collections import namedtuple
from socketio import AsyncClient
from dataclasses import dataclass
import json
import asyncio

Color = NewType('Color', str)

Point = namedtuple('Point', ['x', 'y', 'z'])


@dataclass()
class Node:
    point: Point
    color: Color

    def to_dict(self) -> Dict:
        return {
            'point': {
                'x': self.point.x,
                'y': self.point.y,
                'z': self.point.z
            },
            'color': self.color
        }


class CubeProxy:
    """"""
    IO_EVENTS = {
        'server-change': 'server-change'
    }

    def __init__(self, host: str):
        self.sio = None
        self.host = host

    async def connect(self):
        self.sio = AsyncClient()
        await self.sio.connect(self.host)

    async def send_event(self, *nodes: Node) -> None:
        payload = json.dumps([node.to_dict() for node in nodes])
        await self.sio.emit(CubeProxy.IO_EVENTS['server-change'], payload)

    async def wait(self):
        await self.sio.wait()


async def create_connection(host: str) -> CubeProxy:
    cp = CubeProxy(host)
    await cp.connect()
    return cp


async def main():
    cp = await create_connection('http://localhost:8000')
    await cp.send_event(
        Node(Point(0, 0, 0), '#FF0000'),
        Node(Point(0, 0, 5), '#FF0000'),
        Node(Point(0, 5, 0), '#FF0000'),
        Node(Point(5, 0, 0), '#FF0000'),
        Node(Point(5, 5, 5), '#FF0000'),
        Node(Point(5, 5, 0), '#FF0000'),
        Node(Point(0, 5, 5), '#FF0000'),
        Node(Point(5, 0, 5), '#FF0000')
    )
    await cp.wait()


if __name__ == '__main__':
    asyncio.run(main())
